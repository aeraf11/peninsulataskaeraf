/**
 * Created by Aeraf on 3/17/2019.
 */
app.controller('Files', ['DataService',function (DataService, $q) {
    var vm = this;
    vm.currentPage = 1;
    vm.viewSize = "5";
    vm.myData = DataService.myData;
    vm.sort = {
        column: "type",
        descending: false
    };
    vm.filesData = DataService.filesData;
    angular.forEach(vm.filesData, function (f) {
        if(f.type == 'folder'){
            f.isFolder = 1;
            f.currentPage = 1;
            f.pageSize = "5";
            f.panelOpen = false;
        } else {
            f.isFolder = 0;
        }
    });
    vm.filesData.sort(function (a, b) {
        return a.isFolder - b.isFolder;
    });
    //console.log(vm.filesData);

    vm.onChangePanel = function(f){
        if(f.panelOpen == false) {f.panelOpen = true} else {f.panelOpen = false;}
    }
    vm.changeSort = function(fieldName) {

        vm.filesData.sort(function(a,b){console.log(a[fieldName]); return a[fieldName] - b[fieldName]});
        console.log(fieldName)
    }
    vm.selectedCls = function (column) {
        if (column !== vm.sort.column) {
            return '';
        }
        //vm.paginateItems();
        return column == vm.sort.column && vm.sort.descending ? 'down-arrow' : 'up-arrow';
    };
    vm.changeSorting = function (column) {
        var sort = vm.sort;
        if (sort.column == column) {
            sort.descending = !sort.descending;
        } else {
            sort.column = column;
            sort.descending = false;
        }
        vm.selectedAll = undefined;
    };
    vm.pagerClick = function (button, currentPage, viewSize) {
        switch (button) {
            case "first":
                vm.currentPage = 1;
                break;
            case "prev":
                vm.currentPage--;
                break;
            case "next":
                vm.currentPage++;
                break;
            case "last":
                vm.currentPage = Math.ceil(vm.PagedGrid.length / vm.viewSize);
                break;
        }
    };
    vm.disablePage = function (button) {
        if (vm.filesData) {
            var lastPage = Math.ceil(vm.PagedGrid.length / vm.viewSize);
            switch (button) {
                case "first":
                    return vm.currentPage == 1;
                case "prev":
                    return vm.currentPage == 1;
                case "next":
                    return vm.currentPage == lastPage;
                case "last":
                    return vm.currentPage == lastPage;
                default:
                    return false;
            }
        }
    };

    vm.paginateItems = function (value) {
        var pageSize = parseInt(vm.viewSize);
        var currentPage = parseInt(vm.currentPage);

        return paginateItemsInner(value, currentPage,vm.PagedGrid ,pageSize,vm);
    };

    function paginateItemsInner(value, currentPage, dataItems, viewSize, vm) {
        var begin = (currentPage - 1) * viewSize;
        var end = begin + parseInt(viewSize);
        if (dataItems) {
            if (begin > dataItems.length) {
                begin = 0;
                currentPage = 1;
            }
            if (end > dataItems.length) {
                end = dataItems.length;
            }
            var index = dataItems.indexOf(value);
        }
        return (begin <= index && index < end);
    };

}]);