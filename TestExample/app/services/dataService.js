/**
 * Created by Aeraf on 3/17/2019.
 */
app.factory('DataService', function(){
    this.filesData = [
        {
            "type":"pdf",
            "name":"Employee Handbook",
            "added":"2017-01-06"
        }
        ,{
            "type":"pdf",
            "name":"Public Holiday policy",
            "added":"2016-12-06"
        }
        ,{
            "type":"folder",
            "name":"Expenses",
            "files":[{
                "type":"doc",
                "name":"Expenses claim form",
                "added":"2017-05-02"
            },{
                "type":"doc",
                "name":"Fuel allowances",
                "added":"2017-05-03"
            }]
        }
        ,{
            "type":"folder",
            "name":"Policies",
            "files":[{
                "type":"doc",
                "name":"Policy Handbook",
                "added":"2017-05-02"
            },{
                "type":"doc",
                "name":"Legislations",
                "added":"2017-05-03"
            }]
        }
        ,{
            "type":"folder",
            "name":"Advertising",
            "files":[{
                "type":"doc",
                "name":"List of Media Related",
                "added":"2017-05-02"
            },{
                "type":"doc",
                "name":"Advertising Rules",
                "added":"2017-05-03"
            }]
        }
        ,{
            "type":"folder",
            "name":"Marketing",
            "files":[{
                "type":"doc",
                "name":"List of Advertisers",
                "added":"2017-05-02"
            },{
                "type":"doc",
                "name":"Marketing Rules",
                "added":"2017-05-03"
            }]
        }
        ,{
            "type":"csv",
            "name":"Cost centres",
            "added":"2016-08-12"
        }
        ,{
            "type":"folder",
            "name":"Misc",
            "files":[{
                "type":"doc",
                "name":"Christmas party",
                "added":"2017-12-01"
            },{
                "type":"mov",
                "name":"Welcome to the company!",
                "added":"2015-04-24"
            }]
        }];

    return {
        filesData: this.filesData
    };
});